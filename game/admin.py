from django.contrib import admin

from .models import Character
from .models import CharacterItem
from .models import CharacterKey
from .models import CharacterObject
from .models import CharacterWeapon
from .models import CurrentCharacter
from .models import Item
from .models import Key
from .models import Object
from .models import ObjectChild
from .models import ObjectItem
from .models import Room
from .models import RoomWallObject
from .models import Store
from .models import StoreInventory
from .models import Weapon


admin.site.register(Character)
admin.site.register(CharacterItem)
admin.site.register(CharacterKey)
admin.site.register(CharacterObject)
admin.site.register(CharacterWeapon)
admin.site.register(CurrentCharacter)
admin.site.register(Item)
admin.site.register(Key)
admin.site.register(Object)
admin.site.register(ObjectChild)
admin.site.register(ObjectItem)
admin.site.register(Room)
admin.site.register(RoomWallObject)
admin.site.register(Store)
admin.site.register(StoreInventory)
admin.site.register(Weapon)
