from django.db import models
from django.contrib.auth.models import User


# define the character
class Character(models.Model):
    # set options for sex choice
    choices = ((
        'female',
        'Female'),
        ('male',
         'Male'),
        ('non-binary',
         'Non-Binary'))
    # user is foreign key to User
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    sex = models.CharField(max_length=10, choices=choices)
    created = models.DateField(auto_now_add=True)
    description = models.CharField(max_length=500)
    money = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    level = models.PositiveSmallIntegerField(default=0)
    hit_points = models.PositiveSmallIntegerField(default=10)
    experience = models.IntegerField(default=0)
    room = models.CharField(max_length=250, default='start_room')
    equipped_weapon = models.CharField(max_length=250, default='')
    equipped_item = models.CharField(max_length=250, default='')

# more readable output in admin and shell
    def __str__(self):
        return self.name + ' / ' + self.user.username


class CharacterItem(models.Model):
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    item = models.CharField(max_length=250)

    def __str__(self):
        return (self.character.user.username + ' / ' + self.character.name +
                ' / ' + self.item)


class CharacterKey(models.Model):
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    key = models.CharField(max_length=250)

    def __str__(self):
        return self.character + ' - ' + self.key


class CharacterObject(models.Model):
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    room = models.CharField(max_length=250)
    obj = models.CharField(max_length=250)
    is_locked = models.BooleanField(default=False)
    is_looted = models.BooleanField(default=True)

    def __str__(self):
        return self.character + ' - ' + self.room + '/' + self.obj


class CharacterWeapon(models.Model):
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    weapon = models.CharField(max_length=250)

    def __str__(self):
        return self.character + ' - ' + self.weapon


class CurrentCharacter(models.Model):
    user = models.CharField(max_length=250, unique=True)
    character = models.CharField(max_length=250)

    def __str__(self):
        return self.character + ' - ' + self.user


class Item(models.Model):
    name = models.CharField(max_length=250)
    description = models.CharField(max_length=500)
    stat_affected = models.CharField(max_length=20)
    effect_amt = models.PositiveSmallIntegerField()
    cost = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    sell = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

    def __str__(self):
        return self.name


class Key(models.Model):
    name = models.CharField(max_length=250)
    description = models.CharField(max_length=500)
    room = models.CharField(max_length=250)
    obj = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Object(models.Model):
    name = models.CharField(max_length=250)
    description = models.CharField(max_length=500)

    def __str__(self):
        return self.name


class ObjectChild(models.Model):
    obj = models.ForeignKey(Object, on_delete=models.CASCADE)
    child = models.CharField(max_length=250)

    def __str__(self):
        return self.name + ' / ' + self.child


class ObjectItem(models.Model):
    obj = models.ForeignKey(Object, on_delete=models.CASCADE)
    item = models.CharField(max_length=250)

    def __str__(self):
        return self.obj + ' / ' + self.item


class Room(models.Model):
    choices = (
        ('combat', 'combat'),
        ('store', 'store'),
        ('spawn', 'spawn'),
        ('basic', 'basic'),
    )
    name = models.CharField(max_length=250)
    dark_description = models.CharField(max_length=500)
    light_description = models.CharField(max_length=500)
    room_type = models.CharField(max_length=50, choices=choices)

    def __str__(self):
        return self.name + ' - ' + self.room_type


class RoomWallObject(models.Model):
    choices = (
        ('north', 'north'),
        ('south', 'south'),
        ('east', 'east'),
        ('west', 'west'),
        ('up', 'up'),
        ('down', 'down'),
    )
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    wall = models.CharField(max_length=10, choices=choices)
    obj1 = models.CharField(max_length=250, default='')
    obj2 = models.CharField(max_length=250, default='')
    obj3 = models.CharField(max_length=250, default='')
    obj4 = models.CharField(max_length=250, default='')
    obj5 = models.CharField(max_length=250, default='')


class Store(models.Model):
    name = models.CharField(max_length=250)
    money = models.DecimalField(max_digits=10, decimal_places=2)
    room = models.CharField(max_length=250)

    def __str__(self):
        return self.name + ' - ' + self.room + ' $' + str(self.money)


class StoreInventory(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    item = models.CharField(max_length=250)

    def __str__(self):
        return str(self.store) + ' - ' + self.item


class Weapon(models.Model):
    name = models.CharField(max_length=250)
    description = models.CharField(max_length=500)
    damage = models.PositiveSmallIntegerField()
    accuracy = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return self.name + ' DMG: ' + str(self.damage)
