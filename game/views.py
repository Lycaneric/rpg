# django and python import
from django import forms
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render

# model, forms, and functions import
from .forms import CharacterCreation, UserRegistrationForm
from game import functions
from .models import Character
from .models import CharacterItem
from .models import CurrentCharacter
from .models import Item
from .models import Store
from .models import StoreInventory

# for the date you can use datetime.date.today() or
# datetime.datetime.now().date()
# for the time you can use datetime.datetime.now().time()


def IndexView(request):
    template = 'game/index.html'
    context = functions.FetchContext(request)
    return render(request, template, context)


def signup(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            userObj = form.cleaned_data
            username = userObj['username']
            email = userObj['email']
            password = userObj['password']
            if not (User.objects.filter(username=username).exists() or
                    User.objects.filter(email=email).exists()):
                User.objects.create_user(username, email, password)
                user = authenticate(username=username, password=password)
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                raise forms.ValidationError('Looks like a username with that\
                                            email or password already exists')
    else:
        form = UserRegistrationForm()
    return render(request, 'registration/signup.html', {'form': form})


@login_required
def CreateCharacter(request):
    if request.method == 'POST':
        form = CharacterCreation(request.POST)
        if form.is_valid():
            # process form data
            user = request.user
            if not (Character.objects.filter(name=form.cleaned_data['name'])
                    .exists()):
                name = form.cleaned_data['name']
                sex = form.cleaned_data['sex']
                Character.objects.create(user=user, name=name, sex=sex)
                if not CurrentCharacter.objects.filter(user=user.username):
                    obj = Character.objects.get(name=name)
                    CurrentCharacter.objects.create(user=user.username,
                                                    character=obj.name)
                return HttpResponseRedirect('success')
            else:
                return HttpResponseRedirect('failure')
        else:
            return messages.error(request, 'Error')
    else:
        context = functions.FetchContext(request)
        form = CharacterCreation()
        context['form'] = form
        template = 'game/create.html'
        return render(request, template, context)


@login_required
def DeleteCharacter(request):
    if request.method == 'POST':
        context = functions.FetchContext(request)
        user = User.objects.get(username=context['user'])
        characters = Character.objects.filter(user=user).order_by('created')
        template = 'game/delete.html'
        name = request.POST.get('select_character')
        character = Character.objects.get(name=name)
        character.delete()
        try:
            active = CurrentCharacter.objects.get(
                user=context['user'].username)
            active.delete()
        except:
            pass
        context['characters'] = characters
        return render(request, template, context)
    else:
        context = functions.FetchContext(request)
        user = User.objects.get(username=context['user'])
        characters = Character.objects.filter(user=user).order_by('created')
        template = 'game/delete.html'
        context['characters'] = characters
        return render(request, template, context)


@login_required
def PlayerProfile(request):
    user = request.user
    characters = Character.objects.filter(user=user)
    user_info = User.objects.get(username=user.username)
    template = 'game/pprofile.html'
    context = functions.FetchContext(request)
    context['characters'] = characters
    context['user_info'] = user_info
    return render(request, template, context)


@login_required
def CharacterProfile(request):
    if request.method == 'POST':
        user = request.user
        name = request.POST.get('select_character')
        character = Character.objects.get(name=name)
        if CurrentCharacter.objects.get(user=user.username):
            CurrentCharacter.objects.get(user=user.username).delete()
            current_character = CurrentCharacter.objects.create(
                user=user.username, character=character.name)
            characters = Character.objects.filter(user=user).order_by(
                'created')
            template = 'game/cprofile.html'
            context = functions.FetchContext(request)
            context['active'] = current_character
            context['characters'] = characters
            return render(request, template, context)
        else:
            current_character = CurrentCharacter.objects.create()
            current_character.user = user.username
            current_character.character = character.name
            current_character.save()
            template = 'game/cprofile.html'
            characters = Character.objects.filter(user=user).order_by(
                'created')
            context = functions.FetchContext(request)
            context['active'] = current_character
            context['characters'] = characters
            return render(request, template, context)
    else:
        user = request.user
        template = 'game/cprofile.html'
        characters = Character.objects.filter(user=user).order_by(
            'created')
        try:
            active = CurrentCharacter.objects.get(user=user.username)
        except:
            active = Character.objects.get(user=user).first()
        context = functions.FetchContext(request)
        context['characters'] = characters
        context['active'] = active
    return render(request, template, context)


@login_required
def Game(request):
    template = 'game/game.html'
    context = functions.FetchContext(request)
    return render(request, template, context)


@login_required
def Success(request):
    template = 'game/success.html'
    context = functions.FetchContext(request)
    return render(request, template, context)


@login_required
def Failure(request):
    template = 'game/failure.html'
    context = functions.FetchContext(request)
    return render(request, template, context)


@login_required
def Inventory(request):
    if request.method == 'POST':
        # get username from request
        user = request.user
        # get current character object
        try:
            name = CurrentCharacter.objects.get(user=user.username)
            character = Character.objects.get(name=name.character)
        except:
            character = Character.objects.filter(user=user).first()
        # get item button clicked and its object
        item_name = request.POST.get('item_name')
        item = Item.objects.get(name=item_name)
        if character.equipped_item == item.name:
            character.equipped_item = ''
            character.save()
        else:
            character.equipped_item = item_name
            character.save()
        template = 'game/inventory.html'
        try:
            name = CurrentCharacter.objects.get(user=user.username)
            character = Character.objects.get(name=name.character)
        except:
            character = Character.objects.filter(user=user).first()
        active_char_items = CharacterItem.objects.filter(
            character=character)
        items_list = []
        for i in active_char_items:
            item = i.item
            items_list.append(item)
        items = Item.objects.filter(name__in=items_list)
        context = functions.FetchContext(request)
        context['items'] = items
        context['character'] = character
        return render(request, template, context)
    else:
        user = request.user
        try:
            active = CurrentCharacter.objects.get(user=user.username)
            character = Character.objects.get(name=active.character)
        except:
            character = Character.objects.filter(user=user).first()
        template = 'game/inventory.html'
        active_char_items = CharacterItem.objects.filter(
            character=character)
        items_list = []
        for i in active_char_items:
            item = i.item
            items_list.append(item)
        items = Item.objects.filter(name__in=items_list)
        context = functions.FetchContext(request)
        context['items'] = items
        context['character'] = character
        return render(request, template, context)


@login_required
def StorePage(request):
    if request.method == 'POST' and 'buy' in request.POST:
        # display store items
        fetch = functions.FetchContext(request)
        context = functions.FetchItemLists(request, fetch)
        context['method'] = 'buy'
        template = 'game/store.html'
        return render(request, template, context)
    elif request.method == 'POST' and 'sell' in request.POST:
        # display character items
        fetch = functions.FetchContext(request)
        context = functions.FetchItemLists(request, fetch)
        template = 'game/store.html'
        context['method'] = 'sell'
        return render(request, template, context)
    elif request.method == 'POST' and 'buy_item' in request.POST:
        # buy item from the store
        # get user and active character information
        user = request.user
        active = CurrentCharacter.objects.get(user=user.username)
        character = Character.objects.get(name=active.character)
        # get room and store information
        room = character.room
        store = Store.objects.get(room=room)
        item_name = request.POST.get('buy_item')
        item = Item.objects.get(name=item_name)
        # check if character has enough money
        if character.money >= item.cost:
            # add the item to the characters items
            CharacterItem.objects.create(character=character,
                                         item=item.name)
            # delete the item from the store inventory
            qs = StoreInventory.objects.filter(store=store.pk,
                                               item=item.name)
            qs[0].delete()
            # subtract the item cost
            character.money -= item.cost
            character.save()
            # add the item cost
            store.money += item.cost
            store.save()
        # grab the store and character after update
        room = character.room
        store = Store.objects.get(room=room)
        character = Character.objects.get(name=character.name)
        template = 'game/store.html'
        fetch = functions.FetchContext(request)
        context = functions.FetchItemLists(request, fetch)
        context['method'] = 'buy'
        return render(request, template, context)
    elif request.method == 'POST' and 'sell_item' in request.POST:
        # sell item to store
        # get user and active character information
        user = request.user
        active = CurrentCharacter.objects.get(user=user.username)
        character = Character.objects.get(name=active.character)
        # get room and store information
        room = character.room
        store = Store.objects.get(room=room)
        item_name = request.POST.get('sell_item')
        item = Item.objects.get(name=item_name)
        # check if character has enough money
        if store.money >= item.sell:
            # add the item to the stores items
            StoreInventory.objects.create(store=store,
                                          item=item_name)
            # delete the item from the charavter inventory
            qs = CharacterItem.objects.filter(character=character,
                                              item=item.name)
            qs[0].delete()
            # add to character money item sell price
            character.money += item.sell
            character.save()
            # subtract store money by item sell price
            store.money -= item.sell
            store.save()
        # grab the store and character after update
        room = character.room
        store = Store.objects.get(room=room)
        character = Character.objects.get(name=active.character)
        # setup store inventory
        store_items = StoreInventory.objects.filter(store=store.pk)
        store_inventory = []
        for inv in store_items:
            data = inv.item
            store_inventory.append(data)
        s_items = Item.objects.filter(name__in=store_inventory)
        # setup character inventory
        character_items = CharacterItem.objects.filter(
            character=character)
        character_inventory = []
        for inv in character_items:
            data = inv.item
            character_inventory.append(data)
        c_items = Item.objects.filter(name__in=character_inventory)
        template = 'game/store.html'
        context = functions.FetchContext(request)
        context['s_items'] = s_items
        context['c_items'] = c_items
        context['method'] = 'sell'
        return render(request, template, context)
    else:
        # display buy or sell buttons
        template = 'game/store.html'
        context = functions.FetchContext(request)
        # setup store inventory
        store_items = StoreInventory.objects.filter(store=context['store'].pk)
        store_inventory = []
        for inv in store_items:
            data = inv.item
            store_inventory.append(data)
        s_items = Item.objects.filter(name__in=store_inventory)
        context['s_items'] = s_items
        # setup character inventory
        character_items = CharacterItem.objects.filter(
            character=context['character'])
        character_inventory = []
        for inv in character_items:
            data = inv.item
            character_inventory.append(data)
        c_items = Item.objects.filter(name__in=character_inventory)
        context['c_items'] = c_items
        context['method'] = 'get'
        return render(request, template, context)
