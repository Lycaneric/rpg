from django import forms


class UserRegistrationForm(forms.Form):
    username = forms.CharField(
        required=True,
        label='Username',
        max_length=32
    )
    email = forms.CharField(
        required=True,
        label='Email',
        max_length=250,
    )
    password = forms.CharField(
        required=True,
        label='Password',
        max_length=32,
        widget=forms.PasswordInput()
    )


class CharacterCreation(forms.Form):
    name = forms.CharField(
        max_length=32,
        required=True,
        label='Character Name')
    sex = forms.ChoiceField(
        label='Sex',
        required=True,
        choices=((
            'female',
            'Female'),
            ('male',
             'Male'),
            ('non-binary',
             'Non-Binary'))
        )
