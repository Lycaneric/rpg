from decimal import Decimal
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils import timezone

from .models import Character
from .models import CharacterItem
# from .models import CharacterKey
# from .models import CharacterObject
# from .models import CharacterWeapon
from .models import CurrentCharacter
from .models import Item
# from .models import Key
# from .models import Object
# from .models import ObjectChild
# from .models import ObjectItem
from .models import Room
# from .models import RoomWallObject
from .models import Store
from .models import StoreInventory
# from .models import Weapon


def FetchContext(request):
    if request.user:
        user = request.user
        active_sessions = Session.objects.filter(
            expire_date__gte=timezone.now())
        user_id_list = []
        for session in active_sessions:
            data = session.get_decoded()
            user_id_list.append(data.get('_auth_user_id', None))
        users = User.objects.filter(id__in=user_id_list)
        # get all characters and sum their money
        chars = Character.objects.all().count()
        chars_money = Character.objects.all()
        total_money_found = Decimal(0.00)
        for char in chars_money:
            total_money_found += char.money
        if CurrentCharacter.objects.filter(user=user.username):
            # get user and active character information
            active = CurrentCharacter.objects.get(user=user.username)
            character = Character.objects.get(name=active.character)
            # get room and store information
            room = character.room
            room = Room.objects.get(name=room)
            if room.room_type == 'store':
                store = Store.objects.get(room=room.name)
            else:
                store = ''
            # build the context and return it
            context = {
                'user': user,
                'store': store,
                'room': room,
                'character': character,
                'active': active,
                'total_money_found': total_money_found,
                'users': users,
                'chars': chars,
            }
            return context
        else:
            character = Character.objects.filter(user=user).first()
            CurrentCharacter.objects.create(user=user.username,
                                            character=character.name)
            active = CurrentCharacter.objects.get(character=character.name)
            room = character.room
            room = Room.objects.get(name=room)
            if room.room_type == 'store':
                store = Store.objects.get(room=room.name)
            else:
                store = ''
            active_sessions = Session.objects.filter(
                expire_date__gte=timezone.now())
            user_id_list = []
            for session in active_sessions:
                data = session.get_decoded()
                user_id_list.append(data.get('_auth_user_id', None))
            users = User.objects.filter(id__in=user_id_list)
            # get all characters and sum their money
            chars = Character.objects.all().count()
            chars_money = Character.objects.all()
            total_money_found = Decimal(0.00)
            for char in chars_money:
                total_money_found += char.money
            # build the context and return it
            context = {
                'store': '',
                'room': '',
                'character': character,
                'active': active,
                'total_money_found': total_money_found,
                'users': users,
                'chars': chars,
            }
            return context
    else:
        active_sessions = Session.objects.filter(
            expire_date__gte=timezone.now())
        user_id_list = []
        for session in active_sessions:
            data = session.get_decoded()
            user_id_list.append(data.get('_auth_user_id', None))
        users = User.objects.filter(id__in=user_id_list)
        # get all characters and sum their money
        chars = Character.objects.all().count()
        chars_money = Character.objects.all()
        total_money_found = Decimal(0.00)
        for char in chars_money:
            total_money_found += char.money
        # build the context and return it
        context = {
            'store': '',
            'room': '',
            'character': '',
            'active': '',
            'total_money_found': total_money_found,
            'users': users,
            'chars': chars,
        }
        return context


def FetchItemLists(request, context):
    if Store.objects.filter(name=context['store'].name):
        store_items = StoreInventory.objects.filter(store=context['store'])
        store_inventory = []
        for inv in store_items:
            data = inv.item
            store_inventory.append(data)
        s_items = Item.objects.filter(name__in=store_inventory)
        # setup character inventory
        character_items = CharacterItem.objects.filter(
            character=context['character'])
        character_inventory = []
        for inv in character_items:
            data = inv.item
            character_inventory.append(data)
        c_items = Item.objects.filter(name__in=character_inventory)
        context['s_items'] = s_items
        context['c_items'] = c_items
        return context
    else:
        return context
