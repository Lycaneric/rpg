from django.conf.urls import url
from . import views

app_name = 'game'
urlpatterns = [
    url(r'^$', views.IndexView, name='index'),
    url(r'^create/$', views.CreateCharacter, name='create'),
    url(r'^delete/$', views.DeleteCharacter, name='delete'),
    url(r'^inventory/$', views.Inventory, name='inventory'),
    url(r'^pprofile/$', views.PlayerProfile, name='pprofile'),
    url(r'^cprofile/$', views.CharacterProfile, name='cprofile'),
    url(r'^game/$', views.Game, name='game'),
    url(r'^create/success/$', views.Success, name='success'),
    url(r'^create/failure/$', views.Failure, name='failure'),
    url(r'^store/$', views.StorePage, name='store'),
]
